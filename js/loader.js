$(function() {
	var h = $(window).height();

	$('#wrap').css('display','none');
	$('#loader-bg, #loader').height(h).css('display','block');
});

$(window).load(function () { //すべての読み込みが完了したら実行
	$('#loader-bg').delay(900).fadeOut(800);
	$('#loader').delay(600).fadeOut(300);
	$('#wrap').css('display','block');
});

//5秒たったら強制的にロード画面を非表示
$(function(){
	setTimeout('stopload()',5000);
});

function stopload(){
	$('#wrap').css('display','block');
	$('#loader-bg').delay(900).fadeOut(800);
	$('#loader').delay(600).fadeOut(300);
}